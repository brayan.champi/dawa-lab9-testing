import supertest from 'supertest'
import { app, server } from '../index'

const api = supertest(app)

describe('Test the api/persons', () => {
    test('Notes are returned as JSON', async () => {
        await api
            .get('/api/persons')
            .expect(200)
            .expect('Content-Type', /application\/json/)
    })

    test('There are nighteen persons in the MongoDB collection', async () => {
        const response = await api.get('/api/persons')
        expect(response.body)
    })
})

describe('Tests create new person', () => {
    test('Add new Person', async () => {
        const addPerson = {
            name: 'Arto Hellas',
            numberPhone: '957786155'
        }
        await api.post('/api/person')
            .send(addPerson)
            .expect(201)
            .expect('Content-Type', /application\/json/)
    })
})

describe('Tests find Person', () => {
    test('Person returned as JSON', async () => {
        await api
            .get('/api/persons/Jorge morgan')
            .expect(200)
            .expect('Content-Type', /application\/json/)
    })

    test('Person false', async () => {
        await api
            .get('/api/persons/name')
            .expect(200)
            .expect('Content-Type', /application\/json/)
    })
})

afterAll(() => {
    server.close()
})
