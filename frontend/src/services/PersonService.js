import { SERVIDOR } from './Service'
import axios from 'axios'

class PersonService {
    static urlList = SERVIDOR + '/persons'
    static urlAdd = SERVIDOR + '/person'

    static listPerson = () => axios.get(this.urlList)
    static addPerson = (person) => axios.post(this.urlAdd, person)
    static filterPerson = (name) => axios.get(this.urlList + '/' + name)
}

export default PersonService
