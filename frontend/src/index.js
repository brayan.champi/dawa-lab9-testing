// eslint-disable-next-line no-use-before-define
import React from 'react'
import ReactDOM from 'react-dom'
import {
    BrowserRouter as Router,
    Switch,
    Route
}
    from 'react-router-dom'
import ListPhonebook from './components/listPhonebook'
import FormValidation from './page/FormValidation'

const App = () => {
    return (
        <Router>
            <div>
                <Switch>
                    <Route path="/persons">
                        <ListPhonebook />
                    </Route>
                    <Route path="/person/add/">
                        <FormValidation />
                    </Route>
                </Switch>
            </div>
        </Router>
    )
}

/*ReactDOM.render(<App/>,
    document.getElementById('root')
)*/
export default App
