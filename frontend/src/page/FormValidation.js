// eslint-disable-next-line no-use-before-define
import React, { useState } from 'react'
import {
    ButtonSend,
    ContentButtonCenter,
    Form, MessageCorrect, MessageError
} from '../elements/Form'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faExclamationTriangle } from '@fortawesome/free-solid-svg-icons'
import FormInput from '../components/formInput'
import expressions from '../expressions'
import PersonService from '../services/PersonService'
import './style/style.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import { Link } from 'react-router-dom'

const FormValidation = () => {
    const [name, changeName] = useState({ field: '', valid: null })
    const [phone, changePhone] = useState({ field: '', valid: null })
    const [formValid, changeFormValid] = useState(null)

    const onSubmit = (e) => {
        e.preventDefault()
        if (name.valid === 'true' && phone.valid === 'true'
        ) {
            PersonService.addPerson({
                name: name.field,
                numberPhone: phone.field
            }).then(json => {
                changeFormValid(true)
                changeName({ field: '', valid: null })
                changePhone({ field: '', valid: null })
            })
        } else changeFormValid(false)
    }

    return (
        <main>
            <Link className="btn btn-primary mt-3" to="/persons" >Regresar</Link>
            <Form action="" onSubmit={onSubmit}>
                <FormInput state={name} changeState={changeName}
                    type="text" label="Nombre"
                    placeholder="Brayan Champi" name="nombre_usuario"
                    txtError="El nombre solo puede contener letras y espacios."
                    expresionRegular={expressions.name}/>

                <FormInput state={phone} changeState={changePhone}
                    type="text" label="Teléfono"
                    placeholder="985520369" name="telefono"
                    txtError="El teléfono solo puede contener números y el máximo son 14 dígitos."
                    expresionRegular={expressions.phone}/>

                {formValid === false && <MessageError>
                    <p>
                        <FontAwesomeIcon icon={faExclamationTriangle}/>
                        <b>Error: </b>Por favor rellene el formulario correctamente.
                    </p>
                </MessageError>}
                <ContentButtonCenter>
                    <ButtonSend type="submit">Enviar</ButtonSend>
                    {formValid === true && <MessageCorrect>Formulario enviado exitosamente!</MessageCorrect>}
                </ContentButtonCenter>
            </Form>
        </main>
    )
}

export default FormValidation
