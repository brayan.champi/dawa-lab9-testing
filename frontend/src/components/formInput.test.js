import React from 'react'
import '@testing-library/jest-dom/extend-expect'
import {fireEvent, render } from '@testing-library/react'
import FormInput from './formInput'
import expressions from '../expressions'

describe("FormInput",()=>{
    const handleChange= jest.fn()
    let name = { field: '1', valid: null }
    const changeName = ( algo)=>{
        name=algo
    }
    let component
    beforeEach(()=>{
        component = render(<FormInput state={name} changeState={handleChange}
            type="text" label="Nombre"
            placeholder="Brayan Champi" name="nombre_usuario"
            txtError="El nombre solo puede contener letras y espacios."
            expresionRegular={expressions.name}/>
        )
    })
    test('Se renderiza placeholder del Input',()=>{
        component.getByPlaceholderText('Brayan Champi')
    })
    test('Se ve las veces que se llama cuando se presiona una tecla',()=>{
        let input = component.getByDisplayValue(name.field)
        fireEvent.keyUp(input, { key: 'A', code: 'KeyA' })
        fireEvent.keyUp(input, { key: 'A', code: 'KeyA' })
        expect(handleChange).toHaveBeenCalledTimes(2)
    })
})

