/* eslint-disable react/prop-types */
// eslint-disable-next-line no-use-before-define
import React from 'react'
import { Input, GroupInput, IconValidation, Label, TextError } from '../elements/Form'
import { faCheckCircle, faTimesCircle } from '@fortawesome/free-solid-svg-icons'

const FormInput = (props) => {
    const { state, changeState, type, label, placeholder, name, txtError, expresionRegular, functionPass } = props
    const onChange = (e) => {
        changeState({ ...state, field: e.target.value })
    }
    const validation = () => {
        if (expresionRegular) {
            if (expresionRegular.test(state.field)) {
                changeState({ ...state, valid: 'true' })
            } else {
                changeState({ ...state, valid: 'false' })
            }
        }
        if (functionPass) {
            functionPass()
        }
    }

    return (
        <div>
            <Label htmlFor={name} valid={state.valid}>{label} </Label>
            <GroupInput>
                <Input type={type} placeholder={placeholder} id={name} value={state.field} onChange={onChange}
                    onKeyUp={validation} onBlur={validation} valid={state.valid}/>
                <IconValidation icon={state.valid === 'true' ? faCheckCircle : faTimesCircle} valid={state.valid}/>
            </GroupInput>
            <TextError valid={state.valid}>{txtError}</TextError>
        </div>
    )
}

export default FormInput
