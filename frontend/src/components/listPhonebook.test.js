import React from 'react'
import '@testing-library/jest-dom/extend-expect'
import {render } from '@testing-library/react'
import ListPhonebook from './listPhonebook'

describe("ListPhonebook",()=>{
    test('Se renderiza una table html en la vista',()=>{
        const component = render(<ListPhonebook/>)
        let table = component.getByTestId('table')
        expect(table).toHaveStyle('display:table')
    })
    test('Se renderiza el contenido de ID, Nombre y Número',()=>{
        const component = render(<ListPhonebook/>)
        component.getByText("ID")
        component.getByText("Nombre")
        component.getByText("Número")
    })
})

