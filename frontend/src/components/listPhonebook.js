// eslint-disable-next-line no-use-before-define
import React, { useEffect, useState } from 'react'
import { v4 as uid } from 'uuid'
import { Table } from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUserPlus } from '@fortawesome/free-solid-svg-icons'
import PersonService from '../services/PersonService'
import { Link } from 'react-router-dom'

const ListPhonebook = () => {
    const [phonebookShow, setPhonebookShow] = useState([])
    useEffect(() => {
        //Descomentar para que funcione
        //fetchPhonebook()
    }, [])

    const fetchPhonebook = () => {
        PersonService.listPerson().then(json => {
            if (json.error) console.log('Error')
            setPhonebookShow(json.data)
        })
    }
    const filtrar = (e) => {
        PersonService.filterPerson(e.target.value).then(
            json => {
                if (json.error) console.log('Error')
                console.log(json.data)
                setPhonebookShow(json.data)
            })
    }

    return (
        <div className="container mt-4 col-6">
            {/*Se tiene que descomentar para habilitar la función*/}
            {/*<Link className="btn btn-primary mr-1 mb-4" to="/person/add/">Agregar <FontAwesomeIcon icon={faUserPlus} /></Link>*/}
            <label>Filtro</label>
            <input type="text" onChange={filtrar}/>
            <Table data-testid='table' variant="light">
                <thead className="thead-light">
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Número</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        phonebookShow.data && phonebookShow.data.map((person) => {
                            return (
                                <tr key={uid()}>
                                    <th scope="row">{person.idPerson}</th>
                                    <td>{person.name}</td>
                                    <td>{person.numberPhone}</td>
                                </tr>
                            )
                        })
                    }
                </tbody>
            </Table>
        </div>

    )
}

export default ListPhonebook
